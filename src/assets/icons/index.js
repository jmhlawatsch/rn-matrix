import Check from './icon-check.svg';
import CheckCircle from './icon-check-circle.svg';
import Circle from './icon-circle.svg';
import Close from './icon-close.svg';

export { Check, CheckCircle, Circle, Close };
